<?php

namespace IIcgo\Circe;

class Circe
{
    public $cache = false;
    public $ipdis;

    public function setData()
    {
        if (!$this->cache) {
            $this->ipdis = file_get_contents(__DIR__.'/../data/' . 'lastest.ipns');
        }
    }

    public function outData()
    {
        return file_get_contents(__DIR__.'/../data/' . 'lastest.ipns');
    }

    public function district($ip)
    {
        $ipdis = $this->ipdis;
        $ipex  = explode('.', $ip);

        if (($offset = strpos($ipdis, '.'.$ipex[0].'|')) !== false) {
            $cpos = strpos($ipdis, ':', $offset);
            $anum = strlen($ipex[0]) + 2;
            $nqu  = substr($ipdis, $offset+$anum, $cpos-$offset-$anum).'!';
            $num  = $ipex[1] * 256 + $ipex[2];
            $bnum = 0;
            $off  = 0;
            while ($of = strpos($nqu, '!', $off)) {
                $begin = substr($nqu, $off, $of-$off);
                if ($num < $begin) {
                    return false;
                }

                if ($num == $begin) {
                    return substr($ipdis, $cpos+1+$bnum*4, 4);
                }

                $off = $of+1;
                $of  = strpos($nqu, '!', $off);
                if ($num <= substr($nqu, $off, $of-$off)) {
                    return substr($ipdis, $cpos+1+$bnum*4, 4);
                }
                $off = $of+1;
                $bnum++;
            }
        }
        return false;
    }
}
